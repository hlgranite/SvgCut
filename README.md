# SvgCut
A serial com writer send svg file to plotter for cutting purpose.

## Optional
- [UniConverter 1.1.5](https://sk1project.net/modules.php?name=Products&product=uniconvertor&op=download) for file conversion

## Build with Docker
1. Start docker daemon (service) ``.\dockerd.exe`` in separate Powershell window.
2. Build msbuild image with Dockerfile.
2. Build project with docker.
3. Done.

```
git clone https://github.com/yancyn/SvgCut.git
docker build -t msbuild .
docker run -v "$(pwd):C:\code" microsoft/msbuild msbuild SvgCut.sln /p:Configuration=Release
```

## References
- https://sk1project.net/